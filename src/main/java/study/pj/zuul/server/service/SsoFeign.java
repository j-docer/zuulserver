package study.pj.zuul.server.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "sso-server", path = "/")
public interface SsoFeign {
    /**
     * 判断key是否存在,调用feign的服务。
     */
    @RequestMapping("/redis/hasKey/{key}")
    Boolean hasKey(@PathVariable("key") String key);
}

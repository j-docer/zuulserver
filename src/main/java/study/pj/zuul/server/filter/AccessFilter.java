package study.pj.zuul.server.filter;


import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import study.pj.zuul.server.service.SsoFeign;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccessFilter extends ZuulFilter {

    static Logger logger = LoggerFactory.getLogger(AccessFilter.class);


    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        // PreDecoration之前运行
        return FilterConstants.PRE_DECORATION_FILTER_ORDER - 1;
    }

    @Autowired
    private SsoFeign ssoFeign;

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        logger.info("执行拦截");
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();

        //访问路径
        String url = request.getRequestURL().toString();
        logger.info("访问路径url:" + url);

        // 用户需要带上accessToken
        String accessToken = request.getParameter("accessToken");
        logger.info("token:" + accessToken);
        Cookie[] cookies = request.getCookies();

        // 从cookie中获取accessToken
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if ("accessToken".equals(cookie.getName())) {
                    accessToken = cookie.getValue();
                    logger.info("cookie中的 token:" + accessToken);
                }
            }
        }

        // 过滤规则：cookie有令牌且存在于Redis，或者访问的是登录页面、登录请求则放行：
        // 放行可能：
        // 1、如果是登陆页面，则转发到sso-server中的登陆页面。
        // 2、如果已经登陆：则调用原来url从Zuul转发至服务请求。

        if (url.contains("sso-server/sso/loginPage") || url.contains("sso-server/sso/login") || (!StringUtils.isEmpty(accessToken) && ssoFeign.hasKey(accessToken))) {            // 请求放行
            logger.info("登陆请求！或已登陆过");
            ctx.setSendZuulResponse(true);
            ctx.setResponseStatusCode(200);
            return null;
        } else {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);

            try {
                //　重定向到登录页面。并且记录原来用户的需要请求的URL地址，当用户登陆成功之后，则跳转到
                //　原来的URL地址。
                logger.info("重定向URL：http://localhost:10010/sso/loginPage");
                response.sendRedirect("http://localhost:10010/sso/loginPage?url=" + url);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

package study.pj.zuul.server;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import study.pj.zuul.server.filter.AccessFilter;

@EnableZuulProxy
@SpringBootApplication
@EnableFeignClients
//@EnableEurekaClient
@EnableDiscoveryClient
public class Application {
    static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        logger.info("zuulserver 加载成功");
    }

    @Bean
    public AccessFilter accessFilter() {
        return new AccessFilter();

    }
}
